from django.shortcuts import redirect, render

from todos.forms import TodoForm, TodoItem, TodoItemForm

from .models import TodoList


def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {"todo_list_list": todo_list_list}
    return render(request, "todos/list.html", context)


def show_todo_list(request, pk):
    todo_list = TodoList.objects.get(pk=pk)
    context = {"todo_list": todo_list}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST" and TodoForm:
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", pk=todo_list.pk)
    else:
        form = TodoForm
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, pk):
    instance = TodoList.objects.get(pk=pk)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", pk=pk)
    else:
        form = TodoForm(instance=instance)
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, pk):
    context = {}
    if request.method == "POST":
        todo = TodoList.objects.get(pk=pk)
        todo.delete()
        return redirect(todo_list_list)
    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
    return render(request, "todo_items/create.html", {"form": form})


def todo_item_update(request, pk):
    todo_item = TodoItem.objects.get(pk=pk)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", pk=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)
