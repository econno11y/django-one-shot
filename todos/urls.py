from django.urls import path

from .views import (
    show_todo_list,
    todo_item_create,
    todo_item_update,
    todo_list_create,
    todo_list_delete,
    todo_list_list,
    todo_list_update,
)

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:pk>", show_todo_list, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:pk>/edit/", todo_list_update, name="todo_list_update"),
    path("<int:pk>/delete/", todo_list_delete, name="todo_list_delete"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("items/<int:pk>/edit/", todo_item_update, name="todo_item_update"),
]
